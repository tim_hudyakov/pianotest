package io.github.htim.pianotest.stackexchangeapi.client;

import feign.QueryMap;
import feign.RequestLine;
import io.github.htim.pianotest.stackexchangeapi.model.CommonWrapper;
import io.github.htim.pianotest.stackexchangeapi.model.Question;

import java.util.Map;

/**
 * Created on 02/12/2017.
 */
public interface StackExchangeAPIClient {

    @RequestLine("GET /search?site=stackoverflow&filter=!.FdEuFCavhRSvsz9P.PSdjd7z._N-")
    CommonWrapper<Question> searchQuestions(@QueryMap Map<String, Object> queryMap);

}
