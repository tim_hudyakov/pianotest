package io.github.htim.pianotest.stackexchangeapi.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 02/12/2017.
 */
@Getter
@Setter
public class BadParameterException extends StackExchangeAPIException {

    public BadParameterException(int errorId, String errorMessage, String errorName) {
        super(errorId, errorMessage, errorName);
    }

    public BadParameterException(Throwable cause, int errorId, String errorMessage, String errorName) {
        super(cause, errorId, errorMessage, errorName);
    }
}
