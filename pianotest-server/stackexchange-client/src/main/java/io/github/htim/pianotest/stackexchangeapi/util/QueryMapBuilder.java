package io.github.htim.pianotest.stackexchangeapi.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 02/12/2017.
 */
public class QueryMapBuilder {

    private Map<String, Object> queryMap = new HashMap<>();

    public static QueryMapBuilder instance() {
        return new QueryMapBuilder();
    }

    public QueryMapBuilder withInTitle(String inTitle) {
        this.queryMap.put("intitle", inTitle);
        return this;
    }

    public QueryMapBuilder withPage(Integer page) {
        this.queryMap.put("page", page);
        return this;
    }

    public QueryMapBuilder withPageSize(Integer pageSize) {
        this.queryMap.put("pagesize", pageSize);
        return this;
    }

    public Map<String, Object> build() {
        return this.queryMap;
    }

}
