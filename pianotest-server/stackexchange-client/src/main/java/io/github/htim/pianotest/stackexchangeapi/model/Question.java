package io.github.htim.pianotest.stackexchangeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created on 02/12/2017.
 */
@Data
public class Question {

    @JsonProperty("question_id")
    private int questionId;

    @JsonProperty("creation_date")
    private long creationDate;

    @JsonProperty("title")
    private String title;

    @JsonProperty("link")
    private String link;

    @JsonProperty("is_answered")
    private boolean isAnswered;

    @JsonProperty("owner")
    private User user;

}
