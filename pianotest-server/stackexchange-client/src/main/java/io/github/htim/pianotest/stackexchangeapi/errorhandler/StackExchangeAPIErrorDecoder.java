package io.github.htim.pianotest.stackexchangeapi.errorhandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import io.github.htim.pianotest.stackexchangeapi.exception.BadParameterException;
import io.github.htim.pianotest.stackexchangeapi.exception.InternalErrorException;
import io.github.htim.pianotest.stackexchangeapi.exception.StackExchangeAPIException;
import io.github.htim.pianotest.stackexchangeapi.model.Error;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * Created on 02/12/2017.
 */
@Slf4j
public class StackExchangeAPIErrorDecoder implements ErrorDecoder {

    private ObjectMapper objectMapper;

    public StackExchangeAPIErrorDecoder(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == 400) {
            String body = response.body().toString();
            try {
                Error error = objectMapper.readValue(body, Error.class);
                return getAppropriateException(error);
            } catch (IOException e) {
                log.error("Error occurred while decoding error API response", e);
            }
        }
        return null;
    }

    private StackExchangeAPIException getAppropriateException(Error error) {
        switch (error.getErrorId()) {
            case 400:
                return new BadParameterException(error.getErrorId(), error.getErrorName(), error.getErrorMessage());
            case 500:
                return new InternalErrorException(error.getErrorId(), error.getErrorName(), error.getErrorMessage());
            default:
                return new StackExchangeAPIException(error.getErrorId(), error.getErrorName(), error.getErrorMessage());
        }
    }
}
