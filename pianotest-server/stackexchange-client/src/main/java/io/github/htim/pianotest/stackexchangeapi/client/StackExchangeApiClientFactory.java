package io.github.htim.pianotest.stackexchangeapi.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import io.github.htim.pianotest.stackexchangeapi.errorhandler.StackExchangeAPIErrorDecoder;

/**
 * Created on 04/12/2017.
 */
public class StackExchangeApiClientFactory {

    public static StackExchangeAPIClient getInstance(String baseUrl) {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .errorDecoder(new StackExchangeAPIErrorDecoder(new ObjectMapper()))
                .logger(new Slf4jLogger(StackExchangeAPIClient.class))
                .logLevel(Logger.Level.FULL)
                .target(StackExchangeAPIClient.class, baseUrl);
    }

}
