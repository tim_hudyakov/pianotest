package io.github.htim.pianotest.stackexchangeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created on 02/12/2017.
 */
@Data
public class Error {

    @JsonProperty("error_id")
    private int errorId;

    @JsonProperty("error_message")
    private String errorMessage;

    @JsonProperty("error_name")
    private String errorName;

}
