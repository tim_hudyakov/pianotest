package io.github.htim.pianotest.stackexchangeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created on 02/12/2017.
 */
@Data
public class CommonWrapper<E> {

    @JsonProperty("items")
    private List<E> items;

    @JsonProperty("page")
    private Integer page;

    @JsonProperty("page_size")
    private Integer pageSize;

    @JsonProperty("total")
    private Integer total;

}
