package io.github.htim.pianotest.stackexchangeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created on 02/12/2017.
 */
@Data
public class User {

    @JsonProperty("user_id")
    private int userId;

    @JsonProperty("display_name")
    private String displayName;

    @JsonProperty("link")
    private String link;


}
