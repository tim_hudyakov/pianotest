package io.github.htim.pianotest.stackexchangeapi.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 02/12/2017.
 */
@Getter
@Setter
public class StackExchangeAPIException extends RuntimeException {

    protected int errorId;
    protected String errorMessage;
    protected String errorName;

    public StackExchangeAPIException(int errorId, String errorMessage, String errorName) {
        super(getMessage(errorMessage, errorName));
        this.errorId = errorId;
        this.errorMessage = errorMessage;
        this.errorName = errorName;
    }

    public StackExchangeAPIException(Throwable cause, int errorId, String errorMessage, String errorName) {
        super(getMessage(errorMessage, errorName), cause);
        this.errorId = errorId;
        this.errorMessage = errorMessage;
        this.errorName = errorName;
    }

    private static String getMessage(String errorMessage, String errorName) {
        return String.format("%s: %s", errorMessage, errorName);
    }

}
