package io.github.htim.pianotest.rest.controller;

import io.github.htim.pianotest.dto.Page;
import io.github.htim.pianotest.dto.QuestionDTO;
import io.github.htim.pianotest.service.QuestionSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 02/12/2017.
 */
@RestController
public class QuestionSearchRestController {

    private final QuestionSearchService questionSearchService;

    @Autowired
    public QuestionSearchRestController(QuestionSearchService questionSearchService) {
        this.questionSearchService = questionSearchService;
    }

    @GetMapping("/api/questions")
    public Page<QuestionDTO> findQuestions(
            @RequestParam("inTitle") String inTitle,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer pageSize) {
        return questionSearchService.search(inTitle, page, pageSize);
    }

}
