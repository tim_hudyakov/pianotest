package io.github.htim.pianotest.service;

import io.github.htim.pianotest.dto.Page;
import io.github.htim.pianotest.dto.QuestionDTO;

/**
 * Created on 04/12/2017.
 */
public interface QuestionSearchService {

    Page<QuestionDTO> search(String inTitle, Integer page, Integer pageSize);

}
