package io.github.htim.pianotest.dto;

import lombok.Data;

import java.util.List;


@Data
public class Page<E> {

    private Integer page;
    private Integer pageSize;
    private Integer total;
    private List<E> items;

}
