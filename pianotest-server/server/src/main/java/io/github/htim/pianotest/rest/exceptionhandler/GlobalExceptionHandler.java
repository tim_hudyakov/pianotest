package io.github.htim.pianotest.rest.exceptionhandler;

import io.github.htim.pianotest.stackexchangeapi.exception.BadParameterException;
import io.github.htim.pianotest.stackexchangeapi.exception.InternalErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created on 02/12/2017.
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    private final ErrorAttributes errorAttributes;

    @Autowired
    public GlobalExceptionHandler(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad Request")
    @ExceptionHandler(BadParameterException.class)
    public Map<String, Object> badParameterExceptionHandler(HttpServletRequest request, BadParameterException e) {
        log.error("req: {} {}\nStackexchange API produced an error: ", request.getMethod(), request.getRequestURL(), e);
        return getErrorAttributes(request, false);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalErrorException.class)
    public Map<String, Object> internalErrorExceptionHandler(HttpServletRequest request, InternalErrorException e) {
        log.error("req: {} {}\nStackexchange API produced an error: ", request.getRequestURL(), e);
        return getErrorAttributes(request, false);
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }

}
