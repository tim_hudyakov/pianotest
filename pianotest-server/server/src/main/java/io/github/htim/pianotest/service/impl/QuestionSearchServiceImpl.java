package io.github.htim.pianotest.service.impl;

import io.github.htim.pianotest.dto.Page;
import io.github.htim.pianotest.dto.QuestionDTO;
import io.github.htim.pianotest.mapper.QuestionsMapper;
import io.github.htim.pianotest.service.QuestionSearchService;
import io.github.htim.pianotest.stackexchangeapi.client.StackExchangeAPIClient;
import io.github.htim.pianotest.stackexchangeapi.model.CommonWrapper;
import io.github.htim.pianotest.stackexchangeapi.model.Question;
import io.github.htim.pianotest.stackexchangeapi.util.QueryMapBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

/**
 * Created on 02/12/2017.
 */
@Service
public class QuestionSearchServiceImpl implements QuestionSearchService {

    private final StackExchangeAPIClient client;

    private final QuestionsMapper questionsMapper;

    @Autowired
    public QuestionSearchServiceImpl(StackExchangeAPIClient client, QuestionsMapper questionsMapper) {
        this.client = client;
        this.questionsMapper = questionsMapper;
    }

    public Page<QuestionDTO> search(String inTitle, Integer page, Integer pageSize) {
        Map<String, Object> queryMap = QueryMapBuilder.instance()
                .withInTitle(inTitle)
                .withPage(page)
                .withPageSize(pageSize)
                .build();
        CommonWrapper<Question> questionsCommonWrapper = client.searchQuestions(queryMap);
        List<QuestionDTO> questionDTOs = questionsCommonWrapper.getItems().stream()
                .map(questionsMapper::toQuestionDTO)
                .collect(toList());

        Page<QuestionDTO> questionsPage = new Page<>();

        questionsPage.setItems(questionDTOs);
        questionsPage.setPage(questionsCommonWrapper.getPage());
        questionsPage.setPageSize(questionsCommonWrapper.getPageSize());
        questionsPage.setTotal(questionsCommonWrapper.getTotal());

        return questionsPage;

    }

}
