package io.github.htim.pianotest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.ZonedDateTime;

/**
 * Created on 02/12/2017.
 */
@Data
public class QuestionDTO {

    private int questionId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime creationDate;

    private String title;

    private String link;

    private boolean isAnswered;

    private UserDTO author;

}
