package io.github.htim.pianotest.beans;

import io.github.htim.pianotest.stackexchangeapi.client.StackExchangeAPIClient;
import io.github.htim.pianotest.stackexchangeapi.client.StackExchangeApiClientFactory;
import io.github.htim.pianotest.stackexchangeapi.exception.StackExchangeAPIException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

/**
 * Created on 02/12/2017.
 */
@Configuration
@Slf4j
public class BeansConfiguration {

    @Bean
    public StackExchangeAPIClient stackExchangeAPIClient(@Value("${stackexchange.api.baseurl}") String baseUrl) {
        return StackExchangeApiClientFactory.getInstance(baseUrl);
    }

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {

            @Override
            public Map<String, Object> getErrorAttributes(
                    RequestAttributes requestAttributes,
                    boolean includeStackTrace) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                errorAttributes.remove("timestamp");
                Throwable error = getError(requestAttributes);
                if (error instanceof StackExchangeAPIException) {
                    errorAttributes.put("status", ((StackExchangeAPIException) error).getErrorId());
                    errorAttributes.put("message", error.getMessage());
                }
                return errorAttributes;
            }

        };
    }
}
