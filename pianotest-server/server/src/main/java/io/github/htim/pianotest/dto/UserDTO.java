package io.github.htim.pianotest.dto;

import lombok.Data;

/**
 * Created on 02/12/2017.
 */
@Data
public class UserDTO {

    private Integer userId;
    private String displayName;
    private String link;

}
