package io.github.htim.pianotest.mapper;

import io.github.htim.pianotest.dto.QuestionDTO;
import io.github.htim.pianotest.stackexchangeapi.model.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Created on 02/12/2017.
 */
@Mapper(componentModel = "spring")
public interface QuestionsMapper {

    @Mappings({
            @Mapping(source = "user", target = "author"),
            @Mapping(target = "creationDate", expression = "java(java.time.Instant.ofEpochSecond(question.getCreationDate()).atZone(java.time.ZoneOffset.UTC))")
    })
    QuestionDTO toQuestionDTO(Question question);

}
