package io.github.htim.pianotest.rest;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;


/**
 * Created on 02/12/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integration_test")
@AutoConfigureMockMvc
public class QuestionsSearchRestControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturn400IfInTitleNotSet() throws Exception {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/questions?page=1&pageSize=3", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        JSONAssert.assertEquals(fromClasspath("EmtpyInTitle_Page1_Pagesize3_Response.json"), responseEntity.getBody(), false);
    }

    @Test
    public void shouldReturn200AndQuestionsSet() throws Exception {
        stubFor(get(urlPathEqualTo("/search"))
                .withQueryParam("site", equalTo("stackoverflow"))
                .withQueryParam("filter", equalTo("!.FdEuFCavhRSvsz9P.PSdjd7z._N-"))
                .withQueryParam("intitle", equalTo("Java"))
                .withQueryParam("page", equalTo("1"))
                .withQueryParam("pagesize", equalTo("3"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("Java_Page1_Pagesize3.json")
                )
        );


        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/questions?inTitle=Java&page=1&pageSize=3", String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        JSONAssert.assertEquals(fromClasspath("Java_Page1_Pagesize3_Response.json"), responseEntity.getBody(), false);
    }

    @Test
    public void shouldReturn200AndFirstPageWhenPageNotSet() throws Exception {
        stubFor(get(urlPathEqualTo("/search"))
                .withQueryParam("site", equalTo("stackoverflow"))
                .withQueryParam("filter", equalTo("!.FdEuFCavhRSvsz9P.PSdjd7z._N-"))
                .withQueryParam("intitle", equalTo("Java"))
                .withQueryParam("pagesize", equalTo("3"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("Java_DefaultPage_Pagesize3.json")
                )
        );


        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/questions?inTitle=Java&pageSize=3", String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        JSONAssert.assertEquals(fromClasspath("Java_DefaultPage_Pagesize3_Response.json"), responseEntity.getBody(), false);
    }

    @Test
    public void shouldReturn200AndDefaultPageSizeWhenPageSizeNotSet() throws Exception {
        stubFor(get(urlPathEqualTo("/search"))
                .withQueryParam("site", equalTo("stackoverflow"))
                .withQueryParam("filter", equalTo("!.FdEuFCavhRSvsz9P.PSdjd7z._N-"))
                .withQueryParam("intitle", equalTo("Java"))
                .withQueryParam("page", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBodyFile("Java_Page2_DefaultPageSize.json")
                )
        );


        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/questions?inTitle=Java&page=2", String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        JSONAssert.assertEquals(fromClasspath("Java_Page2_DefaultPageSize_Response.json"), responseEntity.getBody(), false);

    }

    @Test
    public void shouldReturn400AndDescriptionWhenPageIsIncorrect() throws Exception {
        stubFor(get(urlPathEqualTo("/search"))
                .withQueryParam("site", equalTo("stackoverflow"))
                .withQueryParam("filter", equalTo("!.FdEuFCavhRSvsz9P.PSdjd7z._N-"))
                .withQueryParam("intitle", equalTo("Java"))
                .withQueryParam("page", equalTo("-1"))
                .withQueryParam("pagesize", equalTo("3"))
                .willReturn(aResponse()
                        .withStatus(400)
                        .withBodyFile("BadPageParameter.json")
                )
        );


        ResponseEntity<String> responseEntity = restTemplate.getForEntity("/api/questions?inTitle=Java&page=-1&pageSize=3", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        JSONAssert.assertEquals(fromClasspath("BadPageParameter_Response.json"), responseEntity.getBody(), false);

    }


    private String fromClasspath(String fileName) throws IOException {
        return IOUtils.toString(getClass().getClassLoader().getResourceAsStream(fileName), Charset.forName("UTF-8"));
    }
}
