import {FETCH_QUESTIONS_REQUEST, FETCH_QUESTIONS_SUCCESS, FETCH_QUESTIONS_FAILURE} from '../actions/constants';

const initialState = {
    loading: false,
    page: 0,
    items: [],
    err: false,
    total: 0
};

export const questions = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_QUESTIONS_REQUEST: {
            return {
                ...state,
                loading: true,
                page: action.req.page,
                reqString: action.req.reqString
            };
        }
        case FETCH_QUESTIONS_SUCCESS: {
            return {
                ...state,
                loading: false,
                items: action.res.items,
                total: action.res.total
            }
        }
        case FETCH_QUESTIONS_FAILURE: {
            return {
                ...state,
                err: true,
                loading: false
            }
        }
        default: {
            return state;
        }
    }
};
