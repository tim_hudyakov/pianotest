import reducers from '../reducers'
import thunkMiddleware from 'redux-thunk'
import {applyMiddleware, combineReducers, createStore} from "redux";
import {createLogger} from "redux-logger";

export const store = createStore(
    combineReducers({
        questions: reducers.questions
    }),
    applyMiddleware(
        thunkMiddleware,
        createLogger()
    )
);