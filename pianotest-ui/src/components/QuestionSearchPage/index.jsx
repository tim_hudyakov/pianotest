import React, {Component} from 'react';
import {Table, Row, Col, Form, Input} from 'antd'
import moment from "moment";
import queryString from "query-string";
import '../../App.css';

export default class QuestionSearchPage extends Component {

    componentDidMount = () => {
        const queryParams = queryString.parse(this.props.location.search);
        this.props.questionsActions.fetchQuestions(this.extractQueryParams(queryParams));
    };

    componentWillReceiveProps = (nextProps) => {
        const newQueryParams = queryString.parse(nextProps.location.search);
        const queryParams = queryString.parse(this.props.location.search);

        if (queryParams['q'] !== newQueryParams['q']
            || queryParams['page'] !== newQueryParams['page']
            || queryParams['pageSize'] !== newQueryParams['pageSize']) {
            this.props.questionsActions.fetchQuestions(this.extractQueryParams(newQueryParams));
        }

    };

    onSearch = value => {
        const {history, location} = this.props;
        const search = queryString.parse(location.search);
        history.push({
            pathname: location.pathname,
            search: queryString.stringify({
                ...search,
                q: value,
                page: this.defaultPage,
                pageSize: this.defaultPageSize
            })
        });
    };

    onPageChange = page => {
        const {history, location} = this.props;
        const search = queryString.parse(location.search);
        history.push({
            pathname: location.pathname,
            search: queryString.stringify({
                ...search,
                page: page
            })
        });
    };

    onPageSizeChange = (current, pageSize) => {
        const {history, location} = this.props;
        const search = queryString.parse(location.search);
        history.push({
            pathname: location.pathname,
            search: queryString.stringify({
                ...search,
                pageSize: pageSize
            })
        });
    };

    columns = [{
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        width: '60%',
        render: (text, record) => <a href={record.link}>{text}</a>,
    }, {
        title: 'Created',
        dataIndex: 'creationDate',
        key: 'creationDate',
        width: '20%',
        render: text => `${moment(text).local().format("MMM DD 'YY")} at ${moment(text).local().format("HH:mm")}`
    }, {
        title: 'Author',
        dataIndex: 'author.displayName',
        key: 'author.displayName',
        width: '20%',
        render: (text, record) => <a href={record.author.link}>{text}</a>,
    }];

    extractQueryParams = queryParams => {
        return {
            reqString: queryParams['q'],
            page: queryParams['page'] || this.defaultPage,
            pageSize: queryParams['pageSize'] || this.defaultPageSize
        }
    };

    defaultPage = 1;
    defaultPageSize = 10;

    render() {

        const {questions, location} = this.props;
        const queryParams = queryString.parse(location.search);
        const {reqString, page, pageSize} = this.extractQueryParams(queryParams);

        const FormItem = Form.Item;
        const Search = Input.Search;

        return (
            <div>
                <Col span={20} offset={2} style={{background: "white"}}>
                    <Row>
                        <Col span={18} offset={2}>
                            <Form layout="inline">
                                <FormItem>
                                    <Search style={{
                                        paddingBottom: '10px',
                                        paddingTop: '10px'
                                    }}
                                            defaultValue={reqString}
                                            onSearch={this.onSearch}
                                    />
                                </FormItem>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={20} offset={2}>
                            <Table
                                size="small"
                                rowKey="questionId"
                                columns={this.columns}
                                dataSource={questions.items}
                                loading={questions.loading}
                                pagination={{
                                    showSizeChanger: true,
                                    current: parseInt(page),
                                    pageSize: parseInt(pageSize),
                                    pageSizeOptions: ['5', '10', '20', '30', '40'],
                                    onChange: this.onPageChange,
                                    onShowSizeChange: this.onPageSizeChange,
                                    total: questions.total
                                }}
                                rowClassName={(record, index) => {
                                    return record.answered ? 'row_highlited' : '';
                                }}
                            />
                        </Col>
                    </Row>
                </Col>
            </div>
        )
    }
}
