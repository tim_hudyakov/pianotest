import React, {Component} from 'react';
import queryString from "query-string";
import {Input} from "antd";
import '../../App.css';

export default class HomePage extends Component {

    onSearch = value => {
        const {history, location} = this.props;
        const search = queryString.parse(location.search);
        history.push({
            pathname: 'search',
            search: queryString.stringify({
                ...search,
                q: value,
                page: this.defaultPage,
                pageSize: this.defaultPageSize
            })
        });
    };

    defaultPage = 1;
    defaultPageSize = 10;

    render() {
        return (
            <div className="HomePage">
                <div>
                    <h1>Search at Stackoverflow</h1>
                    <Input.Search onSearch={this.onSearch}/>
                </div>
            </div>
        )
    }

}