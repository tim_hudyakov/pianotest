import React, {Component} from 'react';
import './App.css';
import {Provider} from "react-redux";
import {store} from "./store"
import {LocaleProvider} from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'

import ConnectedQuestionSearchPage from "./containers/ConnectedQuestionSearchPage/index";
import HomePage from "./components/HomePage";

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <LocaleProvider locale={enUS}>
                    <Router>
                        <div>
                            <Route exact path="/" component={HomePage}/>
                            <Route path="/search" component={ConnectedQuestionSearchPage}/>
                        </div>
                    </Router>
                </LocaleProvider>
            </Provider>
        );
    }
}

export default App;
