import {connect} from "react-redux";
import {questions} from "../../actions"
import {bindActionCreators} from "redux";
import QuestionSearchPage from "../../components/QuestionSearchPage";

const mapStateToProps = (state) => ({
    questions: state.questions
});

const mapDispatchToProps = (dispatch) => (
    {
        questionsActions: bindActionCreators(questions, dispatch),
    }
);


const ConnectedQuestionSearchPage = connect(mapStateToProps, mapDispatchToProps)(QuestionSearchPage);

export default ConnectedQuestionSearchPage;