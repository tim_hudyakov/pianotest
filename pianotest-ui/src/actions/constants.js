export const FETCH_QUESTIONS_REQUEST = "questions/fetch-request";
export const FETCH_QUESTIONS_FAILURE = "questions/fetch-failure";
export const FETCH_QUESTIONS_SUCCESS = "questions/fetch-success";
