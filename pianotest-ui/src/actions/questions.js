import {FETCH_QUESTIONS_FAILURE, FETCH_QUESTIONS_REQUEST, FETCH_QUESTIONS_SUCCESS} from "./constants";


const fetchQuestionsRequest = (req) => {
    return {
        type: FETCH_QUESTIONS_REQUEST,
        req
    }
};

const fetchQuestionsSuccess = (res) => {
    return {
        type: FETCH_QUESTIONS_SUCCESS,
        res
    }
};

const fetchQuestionsFailure = (err) => {
    return {
        type: FETCH_QUESTIONS_FAILURE,
        err
    }
};

export const fetchQuestions = (req) => {
    return dispatch => {
        dispatch(fetchQuestionsRequest(req));
        const {page = 1, pageSize = 5, reqString = ""} = req;
        const API = `/api/questions?inTitle=${reqString}&page=${page}&pageSize=${pageSize}`;
        return fetch(API)
            .then(response => {
                if (response.status !== 200) {
                    dispatch(fetchQuestionsFailure({}))
                } else {
                    return response.json()
                }
            })
            .then(json => {
                dispatch(fetchQuestionsSuccess(json))
            })
            .catch(err => {
                dispatch(fetchQuestionsFailure(err))
            })


    }
}